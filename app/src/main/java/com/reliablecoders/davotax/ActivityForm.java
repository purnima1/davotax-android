package com.reliablecoders.davotax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.Exception;
import java.util.StringTokenizer;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


/**
 * Created by shawn on 9/17/14.
 */
public class ActivityForm extends Activity {
    EditText businessName, businessAddress, businessAddressTwo, businessCity, businessState,
            businessPostal, businessMerchantKey, contactPhone, contactEmail, contactEin, contactOwnerFirst,
            contactOwnerMiddle, contactOwnerLast, bankBusinessName, bankName, bankRouting, bankChecking,
            stateFilingAddress, stateFilingAddressTwo, stateFilingCity, stateFilingState, stateFilingPostal,
            stateFilingUsername, stateFilingPassword, stateFilingId;
    Button submit;
    TextView businessMerchantKeyTextView;


    String mToken, mMerchantId, mEmployeeId, mAccessToken;

    String mCurBusName, mCurBusAddress, mCurBusAddressTwo, mCurBusAddressThree, mCurBusCity, mCurBusCountry, mCurBusState,
            mCurBusPostal, mCurBusPone, mCurBusEmail, mCurBusFirst, mCurBusMid, mCurBusLast,
            mAuthToken;


    public static final String AUTH_CODE = "auth_code";
    public static final String MERCHANT_ID_KEY = "merchant_id";
    public static final String EMPLOYEE_ID_KEY = "employee_id";
    public static final String ACCESS_TOKEN = "access_token";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        Intent infoIntent = getIntent();
        mMerchantId = infoIntent.getStringExtra("mCurMurId");
        mCurBusName = infoIntent.getStringExtra("mCurBusName");
        mCurBusAddress = infoIntent.getStringExtra("mCurBusAddress");
        mCurBusAddressTwo = infoIntent.getStringExtra("mCurBusAddressTwo");
        mCurBusCity = infoIntent.getStringExtra("mCurBusCity");
        mCurBusCountry = infoIntent.getStringExtra("mCurBusCountry");
        mCurBusPone = infoIntent.getStringExtra("mCurBusPone");
        mCurBusState = infoIntent.getStringExtra("mCurBusState");
        mCurBusPostal = infoIntent.getStringExtra("mCurBusPostal");
        mCurBusEmail = infoIntent.getStringExtra("ownerEmail");
        mAuthToken =  infoIntent.getStringExtra("authToken");

        businessName = (EditText) findViewById(R.id.et_business_name);
        businessAddress = (EditText) findViewById(R.id.et_business_address);
        businessAddressTwo = (EditText) findViewById(R.id.et_business_address_two);
        businessCity = (EditText) findViewById(R.id.et_business_city);
        businessState = (EditText) findViewById(R.id.et_business_state);
        businessPostal = (EditText) findViewById(R.id.et_business_postal);
        businessMerchantKey = (EditText) findViewById(R.id.et_business_merchant_api_key);
        businessMerchantKey.setVisibility(View.GONE);
        businessMerchantKeyTextView = (TextView) findViewById(R.id.tv_business_merchant_api_key);
        businessMerchantKeyTextView.setVisibility(View.GONE);

        contactPhone = (EditText) findViewById(R.id.et_business_phone);
        contactEmail = (EditText) findViewById(R.id.et_business_email);
        contactEin = (EditText) findViewById(R.id.et_business_ein);
        contactOwnerFirst = (EditText) findViewById(R.id.et_business_owner_first);
        contactOwnerMiddle = (EditText) findViewById(R.id.et_business_owner_middle);
        contactOwnerLast = (EditText) findViewById(R.id.et_business_owner_last);
        bankBusinessName = (EditText) findViewById(R.id.et_bank_business_name);
        bankName = (EditText) findViewById(R.id.et_bank_name);
        bankRouting = (EditText) findViewById(R.id.et_bank_routing_number);
        bankChecking = (EditText) findViewById(R.id.et_bank_account_number);
        submit = (Button) findViewById(R.id.btn_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInfoAndSend();
            }
        });


//        getAndSetInfoFromServer();
        fillInfoFromServer();
        fillMoreInfoFromServer();
    }

    private void fillInfoFromServer() {
        businessMerchantKey.setText(mMerchantId);
        businessAddress.setText(mCurBusAddress);
        businessAddressTwo.setText(mCurBusAddressTwo);
        businessCity.setText(mCurBusCity);
        businessState.setText(mCurBusState);
        businessPostal.setText(mCurBusPostal);
        contactPhone.setText(mCurBusPone);
    }

    private void fillMoreInfoFromServer() {
        businessName.setText(mCurBusName);
    }

//    private void getAndSetInfoFromServer() {
//
//        Intent fromStart = getIntent();
//        mToken = fromStart.getStringExtra(AUTH_CODE);
//        mMerchantId = fromStart.getStringExtra(MERCHANT_ID_KEY);
//
//        mEmployeeId = fromStart.getStringExtra(EMPLOYEE_ID_KEY);
//        mAccessToken = fromStart.getStringExtra(ACCESS_TOKEN);


//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                // try {
//                try {
//                    HttpClient httpclient = new DefaultHttpClient();
//
//                    String website = "https://api.clover.com/v3/merchants/" + mMerchantId;// + "/address";//+"?access_token="+"["+mAccessToken+"]";
//                    String website2 = "https://api.clover.com/v3/merchants/" + mMerchantId;//+"?access_token="+"["+mAccessToken+"]";
//                    HttpGet request = new HttpGet(website);
//                    HttpGet request2 = new HttpGet(website2);
//                    request.setHeader("Authorization", "Bearer " + mAccessToken);
//                    request2.setHeader("Authorization", "Bearer " + mAccessToken);
////            request.setURI(website);
//                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
////            HttpResponse response = httpclient.execute(request, responseHandler);
//                    String rsp2 = httpclient.execute(request2, responseHandler);
//                    try {
//                        JSONObject jObj2 = new JSONObject(rsp2);
//                        try{
//                            String busName = jObj2.getString("name");
//                            mCurBusName = busName;
//                        }catch(JSONException e){
//                        }
//                        try{
//                            JSONObject ownerObject = jObj2.getJSONObject("owner");
//                            try{
//                                String ownerEmail = ownerObject.getString("email");
//                                contactEmail.setText(ownerEmail);
//                            }catch (JSONException e){}
//                        }catch(JSONException e){}
//                        /*
//                        {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2", "id": "RZ0PTQE3AXJK2", "name": "Home corp",
//                         "owner": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/employees/WVM1RHHXNGQW6", "id": "WVM1RHHXNGQW6",
//                          "orders": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/employees/WVM1RHHXNGQW6/orders"}},
//                           "address":
//                          {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/address"}, "phoneNumber": "67555555555", "createdTime": 1398972777000,
//                          "tenders": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/tenders"}, "shifts": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/shifts"},
//                          "orders": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/orders"}, "payments": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/payments"},
//                          "taxRates": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/tax_rates"}, "printers": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/printers"},
//                          "modifierGroups": {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/modifier_groups"}, "orderTypes":
//                          {"href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/order_types"}, "reseller": {"id": "EKJGCMKBTDK4A"}}
//                         */
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                fillMoreInfoFromServer();
//                            }
//                        });
//                    }catch (Throwable t) {
////                        Log.e("DAVOTAX", "Could not parse malformed JSON: \"" + rsp2 + "\"");
//                    }
//                    String rsp = httpclient.execute(request, responseHandler);
//                    try {
//                        JSONObject jObj = new JSONObject(rsp);
////                        Log.d("activityForm", jObj.toString());
//                        /*if(jObj.has("lala")){
//                            Toast.makeText(ActivityForm.this, "lala", Toast.LENGTH_SHORT).show();
//                        }else{
//                            Toast.makeText(ActivityForm.this, "lele", Toast.LENGTH_SHORT).show();
//                        }*/
//                        try {
//                            String address1 = jObj.getString("address1");
//                            mCurBusAddress = address1;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String address2 = jObj.getString("address2");
//                            mCurBusAddressTwo = address2;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String city = jObj.getString("city");
//                            mCurBusCity = city;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String country = jObj.getString("country");
//                            mCurBusCountry = country;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String phoneNumber = jObj.getString("phoneNumber");
//                            mCurBusPone = phoneNumber;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String state = jObj.getString("state");
//                            mCurBusState = state;
//                        }catch(JSONException e){
//                        }
//                        try {
//                            String zip = jObj.getString("zip");
//                            mCurBusPostal = zip;
//                        }catch(JSONException e){
//                        }
//
////                        String address3 = jObj.getString("address3");
////                        mCurBusAddressThree = address3;
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                fillInfoFromServer();
//                            }
//                        });
//                    } //catch (Throwable t) {
////                        Log.e("DAVOTAX", "Could not parse malformed JSON: \"" + rsp + "\"");
//                    /*}*/ catch (JSONException e) {
//                        //
//                    }
//
//            /*
//            {
//    "href": "https://www.clover.com/v3/merchants/RZ0PTQE3AXJK2/address",
//    "address1": "1000 Mathilda Ave",
//    "address2": "Suite 2342",
//    "address3": "Madera Court",
//    "city": "Sunnyvale",
//    "country": "US",
//    "phoneNumber": "67555555555",
//    "state": "California",
//    "zip": "94086"
//}
//             */
////                    Log.d("DAVOTAX", rsp);
//
////        }catch(Exception e){
////            if(e!=null) {
////                Log.e("log_tag", "Error in http connection " + e.toString());
////            }
//                } catch (IOException l) {
////                    Log.e("DAVOTAX", "Error in http connection " + l.toString());
//                }
//            }
//        };
//        android.os.Process.setThreadPriority(999);
//        thread.start();
//
//
//
//    }



    private void getInfoAndSend() {


        Toast.makeText(ActivityForm.this, "Loading... Please wait.", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"shawnh@reliablecoders.com"});//support@davotechnologies.com
//        intent.setData(Uri.parse("shawnh@reliablecoders.com"));
//        //From: cloverapp@davotechnologies.com
//        intent.putExtra(Intent.EXTRA_SUBJECT, "Clover Account: New User Registered");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setType("message/rfc822");
//        intent.setType("application/text");
        final String emailText =
                getResources().getString(R.string.business_name) + " : " + businessName.getText().toString() + "\n\n" +
                        getResources().getString(R.string.address) + " : " + businessAddress.getText().toString() + "\n\n" +
                        getResources().getString(R.string.address_line_two) + " : " + businessAddressTwo.getText().toString() + "\n\n" +
                        getResources().getString(R.string.city) + " : " + businessCity.getText().toString() + "\n\n" +
                        getResources().getString(R.string.state) + " : " + businessState.getText().toString() + "\n\n" +
                        getResources().getString(R.string.postal_code) + " : " + businessPostal.getText().toString() + "\n\n" +
                        getResources().getString(R.string.clover_merchant_api) + " : " + mAuthToken + "\n\n" +
                        getResources().getString(R.string.phone) + " : " + contactPhone.getText().toString() + "\n\n" +
                        getResources().getString(R.string.email) + " : " + contactEmail.getText().toString() + "\n\n" +
                        getResources().getString(R.string.ein) + " : " + contactEin.getText().toString() + "\n\n" +
                        getResources().getString(R.string.first) + " : " + contactOwnerFirst.getText().toString() + "\n\n" +
                        getResources().getString(R.string.middle) + " : " + contactOwnerMiddle.getText().toString() + "\n\n" +
                        getResources().getString(R.string.last) + " : " + contactOwnerLast.getText().toString() + "\n\n" +
                        getResources().getString(R.string.business_name_as_appears) + " : " + bankBusinessName.getText().toString() + "\n\n" +
                        getResources().getString(R.string.bank_name) + " : " + bankName.getText().toString() + "\n\n" +
                        getResources().getString(R.string.routing_number) + " : " + bankRouting.getText().toString() + "\n\n" +
                        getResources().getString(R.string.checking_number) + " : " + bankChecking.getText().toString() + "\n\n" ;

// ppp        getResources().getString(R.string.clover_merchant_api) + " : " + businessMerchantKey.getText().toString() + "\n\n" +
//        intent.putExtra(Intent.EXTRA_TEXT, emailText);
//        startActivity(intent);
//        try {
//            startActivity(Intent.createChooser(intent, "Send mail..."));
//        } catch (android.content.ActivityNotFoundException ex) {
//            Toast.makeText(ActivityForm.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//        }

        Thread thread = new Thread() {
            @Override
            public void run() {

                try {
                    GMailSender sender = new GMailSender("mailapk@davotechnologies.com", "$%^Clover123");
                    sender.sendMail("Clover Account: New User Registered",
                            emailText,
                            "mailapk@davotechnologies.com",
                            "support@davotechnologies.com");//support@davotechnologies.com
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivityForm.this, "Your form has been submitted. Our representative will be in contact with you soon.", Toast.LENGTH_LONG).show();
                        }
                    });

                    String forwardMerchantId = mMerchantId; // businessMerchantKey.getText().toString();
                    String backwardsMechantId = "";

                    for (int i = forwardMerchantId.length(); i > 0; i--) {
                        backwardsMechantId += forwardMerchantId.charAt(i-1);
                        Log.e("merchant id backwards", backwardsMechantId);
                    }
                    WebServiceActivity.CustomHttpClient httpClient = WebServiceActivity.CustomHttpClient.getHttpClient();
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String cmCheckUrl = "http://www.davotechnologies.com/app/cmcheck.php"+"?merchant_id="+forwardMerchantId+"&apival="+backwardsMechantId +"&form_complete=1";
                    HttpGet cmCheckRequest = new HttpGet(cmCheckUrl);
//                        request.setHeader("Authorization", "Bearer " + authResult.authToken);
                    String cmCheckResult = httpClient.execute(cmCheckRequest, responseHandler);
//                    JSONObject cmCheckAddressObject = new JSONObject(cmCheckResult);
                    Intent finishIntent = new Intent(ActivityForm.this, ActivityFinish.class);
                    startActivity(finishIntent);
                    finish();
                } catch (Exception e) {
//                    Log.e("SendMail", e.getMessage(), e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivityForm.this, "Email send failed, check your connection and try again.", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }

        };
        android.os.Process.setThreadPriority(999);
        thread.start();
    }

    @Override
    public void onStart() {
        // RUN SUPER | REGISTER ACTIVITY AS INSTANTIATED IN APP CLASS

        super.onStart();
        App.activity3 = this;
    }

    @Override
    public void onDestroy() {
        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS

        super.onDestroy();
        App.activity3 = null;
    }
}
