package com.reliablecoders.davotax;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by purnimam on 12/16/14.
 */
public class FAQWebViewActivity extends Activity {

    private WebView mWebView;
    String mUrl = "http://www.davotechnologies.com/app/help.php";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqwebview);

        mWebView = (WebView) findViewById(R.id.wvFAQ);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mUrl);

    }

}
