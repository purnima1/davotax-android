package com.reliablecoders.davotax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ActivityStart extends Activity {

    public static final int OAUTH_REQUEST_CODE = 0;

    public static final String AUTH_CODE = "auth_code";
    public static final String MERCHANT_ID_KEY = "merchant_id";
    public static final String EMPLOYEE_ID_KEY = "employee_id";
    public static final String ACCESS_TOKEN = "access_token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Intent webIntent = new Intent(ActivityStart.this, WebServiceActivity.class);
        startActivity(webIntent);
        finish();
//        startActivityForResult(webIntent, OAUTH_REQUEST_CODE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if(requestCode == OAUTH_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
//
//            // Access data from the completed intent
//            String token = data.getStringExtra(AUTH_CODE);
//            String merchantId = data.getStringExtra(MERCHANT_ID_KEY);
//            String employeeId = data.getStringExtra(EMPLOYEE_ID_KEY);
//            String accessToken = data.getStringExtra(ACCESS_TOKEN);
//
//            Intent formIntent = new Intent(ActivityStart.this, ActivityForm.class);
//            formIntent.putExtra(AUTH_CODE, token);
//            formIntent.putExtra(MERCHANT_ID_KEY, merchantId);
//            formIntent.putExtra(EMPLOYEE_ID_KEY, employeeId);
//            formIntent.putExtra(ACCESS_TOKEN, accessToken);
//            startActivity(formIntent);
//        }
//        else {
//            Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onStart()
    {
        // RUN SUPER | REGISTER ACTIVITY AS INSTANTIATED IN APP CLASS

        super.onStart();
        App.activity1 = this;
    }

    @Override
    public void onDestroy()
    {
        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS

        super.onDestroy();
        App.activity1 = null;
    }
}
