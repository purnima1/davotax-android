package com.reliablecoders.davotax;

import android.app.Activity;

/**
 * Created by shawn on 9/18/14.
 */
public class App
{
    ////////////////////////////////////////////////////////////////
    // INSTANTIATED ACTIVITY VARIABLES
    ////////////////////////////////////////////////////////////////

    public static Activity activity1;
    public static Activity activity2;
    public static Activity activity3;
    public static Activity activity4;
    public static Activity activity5;

    ////////////////////////////////////////////////////////////////
    // CLOSE APP METHOD
    ////////////////////////////////////////////////////////////////

    public static void close()
    {
        if (App.activity5 != null) {App.activity5.finish();}
        if (App.activity4 != null) {App.activity4.finish();}
        if (App.activity3 != null) {App.activity3.finish();}
        if (App.activity2 != null) {App.activity2.finish();}
        if (App.activity1 != null) {App.activity1.finish();}
    }
}