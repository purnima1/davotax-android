package com.reliablecoders.davotax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

/**
 * Created by shawn on 9/18/14.
 */

public class ActivityFinish extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        Button finish = (Button)findViewById(R.id.btn_close);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               App.close();
               finish();
            }
        });
        Button faq = (Button)findViewById(R.id.btn_faq);
        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent faqIntent = new Intent(ActivityFinish.this, FAQWebViewActivity.class);
                startActivity(faqIntent);
            }
        });

    }

    @Override
    public void onStart()
    {
        // RUN SUPER | REGISTER ACTIVITY AS INSTANTIATED IN APP CLASS

        super.onStart();
        App.activity4 = this;
    }

    @Override
    public void onDestroy()
    {
        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS

        super.onDestroy();
        App.activity4 = null;
    }
}
