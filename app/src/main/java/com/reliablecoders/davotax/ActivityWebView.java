//package com.reliablecoders.davotax;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.util.Log;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.ResponseHandler;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.BasicResponseHandler;
//import org.apache.http.impl.client.DefaultHttpClient;
//
///**
// * Created by shawn on 9/17/14.
// */
//public class ActivityWebView extends Activity {
//    String url1;
//    String apiToken;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        super.setContentView(R.layout.activity_webview);
//
//        WebView webView;
//
//        // The URL that will fetch the Access Token, Merchant ID, and Employee ID
//        url1 = "https://clover.com/oauth/authorize" +
//                "?client_id=" + getResources().getString( R.string.app_id)+
//                "&redirect_uri=" + getResources().getString( R.string.app_domain);
//
//        // Creates the WebView
//        webView = (WebView) findViewById(R.id.webView);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setWebViewClient(new WebViewClient() {
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                // Parses the fetched URL
//                String merchantIdFragment = "?merchant_id=";
//                String employeeIdFragment = "&employee_id=";
//                String clientIdFragment = "&client_id=";
//                String authCodeFragment = "&code=";
//
//                int merchantIdStart = url.indexOf(merchantIdFragment);
//                int employeeIdStart = url.indexOf(employeeIdFragment);
//                int clientIdStart = url.indexOf(clientIdFragment);
//                int authCodeStart = url.indexOf(authCodeFragment);
//                if (authCodeStart > -1) {
//                    final String merchantId = url.substring(merchantIdStart + merchantIdFragment.length(), employeeIdStart);
//                    final String employeeId = url.substring(employeeIdStart + employeeIdFragment.length(), clientIdStart);
//                    final String clientId = url.substring(clientIdStart + clientIdFragment.length(), authCodeStart);
//                    final String accessToken = url.substring(authCodeStart + authCodeFragment.length(), url.length());
//
//                    Thread thread = new Thread()
//                    {
//                        @Override
//                        public void run() {
//                            // try {
//                            try{
//                                HttpClient httpclient = new DefaultHttpClient();
//
//                                String website = "https://www.clover.com/oauth/token"+"?client_id=BE89EAS9QYPMG"+
//                                        "&client_secret=dbbc896b-ea68-d87b-818f-57d17506943c"+"&code="+accessToken;
//                                HttpGet request = new HttpGet(website);
////            request.setURI(website);
//                                ResponseHandler<String> responseHandler = new BasicResponseHandler();
////            HttpResponse response = httpclient.execute(request, responseHandler);
//                                String rsp = httpclient.execute(request, responseHandler);
//                                String accessTokenFragment = ("\"access_token\":\"");
//                                int accessTokenStart = rsp.indexOf(accessTokenFragment);
//                                apiToken = rsp.substring(accessTokenStart + accessTokenFragment.length(),rsp.length()-2);
////                                Log.d("DAVOTAX", rsp);//{"access_token":"fc9d2d48-fff7-aeb4-81c4-24f45a0092ec"}
//
//                                Intent output = new Intent();
//                                output.putExtra(ActivityForm.AUTH_CODE, accessToken);
//                                output.putExtra(ActivityForm.MERCHANT_ID_KEY, merchantId);
//                                output.putExtra(ActivityForm.EMPLOYEE_ID_KEY, employeeId);
//                                output.putExtra(ActivityForm.ACCESS_TOKEN, apiToken);
//                                setResult(RESULT_OK, output);
//                                finish();
//
//                            }catch(Exception e){
////                                Log.e("log_tag", "Error in http connection " + e.toString());
//                            }
//                        }
//                    };
//                    android.os.Process.setThreadPriority(999);
//                    thread.start();
//
//
//                }
//            }
//        });
//        // Loads the WebView
//        webView.loadUrl(url1);
//    }
//
//
//    @Override
//    public void onStart()
//    {
//        // RUN SUPER | REGISTER ACTIVITY AS INSTANTIATED IN APP CLASS
//
//        super.onStart();
//        App.activity2 = this;
//    }
//
//    @Override
//    public void onDestroy()
//    {
//        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS
//
//        super.onDestroy();
//        App.activity2 = null;
//    }
//}