package com.reliablecoders.davotax;

import android.accounts.Account;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clover.sdk.util.CloverAccount;
import com.clover.sdk.util.CloverAuth;
import com.clover.sdk.v1.BindingException;
import com.clover.sdk.v1.ClientException;
import com.clover.sdk.v1.ServiceException;
import com.clover.sdk.v1.merchant.Merchant;
import com.clover.sdk.v1.merchant.MerchantConnector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WebServiceActivity extends Activity {

    private static final String TAG = WebServiceActivity.class.getSimpleName();

    private Account mAccount;
//    private Account[] mAccountList;
    private Button mButton; // for  mbutton
    private TextView mLogText;
    private String mCurBusName, mCurBusAddress, mCurBusAddressTwo, mCurMurId,
            mCurBusCity, mCurBusCountry, mCurBusPone, mCurBusState, mCurBusPostal, ownerEmail;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("H:mm:ss");
    private MerchantConnector mMerchantConnector;
    private String mAuthToken=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_service);

        mLogText = (TextView) findViewById(R.id.logText);
        queryWebService();
        mButton = (Button) findViewById(R.id.button);
        mButton.setVisibility(View.GONE);
        mButton.setOnClickListener(mRequestOnClickListener);
        mAccount = CloverAccount.getAccount(this);

        if (mAccount == null) {
            log("Account not found.");
            return;
        }
        log("Retrieved Clover Account: " + mAccount.name);

       // Create and Connect to the MerchantConnector
//ppp      connect();

       // Get the merchant object
//ppp      getMerchant();
    }

    private void connect() {
        disconnect();
        if (mAccount != null) {
            mMerchantConnector = new MerchantConnector(this, mAccount, null);
            mMerchantConnector.connect();
        }
    }

    private void disconnect() {
        if (mMerchantConnector != null) {
            mMerchantConnector.disconnect();
            mMerchantConnector = null;
        }
    }

    private View.OnClickListener mRequestOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            queryWebService();
            v.setEnabled(false);
        }
    };

    private void log(String text) {
        Log.i(TAG, text);

        StringBuilder sb = new StringBuilder(mLogText.getText().toString());
        if (sb.length() > 0) {
            sb.append('\n');
        }
        sb.append(dateFormat.format(new Date())).append(": ").append(text);
        mLogText.setText(sb.toString());
    }

    private void queryWebService() {
        new AsyncTask<Void, String, Void>() {
            @Override
            protected void onProgressUpdate(String... values) {
                String logString = values[0];
                log(logString);
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    publishProgress("Requesting auth token");
                    CloverAuth.AuthResult authResult = CloverAuth.authenticate(WebServiceActivity.this, mAccount);

//                    String authString = authResult.toString();
                    publishProgress("Successfully authenticated as " + mAccount + ".  authToken=" + authResult.authToken + ", authData=" + authResult.authData);
//                    Bundle authDataBundle = authResult.authData;

                    if (authResult.authToken != null && authResult.baseUrl != null) {


                        CustomHttpClient httpClient = CustomHttpClient.getHttpClient();


                        ResponseHandler<String> responseHandler = new BasicResponseHandler();

                        String forwardMerchantId = authResult.merchantId;
                        String backwardsMechantId = "";
                        mAuthToken = authResult.authToken;

                        for (int i = forwardMerchantId.length(); i > 0; i--) {
                            backwardsMechantId += forwardMerchantId.charAt(i-1);
                            Log.e("merchant id backwards", backwardsMechantId);
                        }
                        boolean isCheckBool = false;
                        String cmCheckUrl = "http://www.davotechnologies.com/app/cmcheck.php"+"?merchant_id="+authResult.merchantId+"&apival="+backwardsMechantId;
                        HttpGet cmCheckRequest = new HttpGet(cmCheckUrl);
//                        request.setHeader("Authorization", "Bearer " + authResult.authToken);
                        publishProgress("requesting merchant using: " + cmCheckUrl);
                        String cmCheckResult = httpClient.execute(cmCheckRequest, responseHandler);
                        JSONObject cmCheckAddressObject = new JSONObject(cmCheckResult);
                        try {
                            int checkInt = cmCheckAddressObject.getInt("is_registered");
                            switch (checkInt){
                                case 0:
                                    isCheckBool = false;
                                    break;
                                case 1:
                                    isCheckBool = true;
                                    break;
                            }
                        } catch (JSONException e) {
                        }
                        if(isCheckBool){
                            Intent finishIntent = new Intent(WebServiceActivity.this, ActivityFinish.class);
                            startActivity(finishIntent);
                            finish();
                        }else{


                        String getMerchantUri = "/v3/merchants/";//name";
                        String url = authResult.baseUrl + getMerchantUri + authResult.merchantId;//+"/address";
                        HttpGet request = new HttpGet(url);
                        request.setHeader("Authorization", "Bearer " + authResult.authToken);
                        publishProgress("requesting merchant using: " + url);
                        String result = httpClient.execute(request, responseHandler);

                        CustomHttpClient httpClient2 = CustomHttpClient.getHttpClient();
                        String getAddressUri2 = "/v3/merchants/";//name";
                        String url2 = authResult.baseUrl + getAddressUri2 + authResult.merchantId+"/address";
                        HttpGet request2 = new HttpGet(url2);
                        request2.setHeader("Authorization", "Bearer " + authResult.authToken);
                        publishProgress("requesting merchant using: " + url2);
                        String result2 = httpClient2.execute(request2, responseHandler);

//                            JSONObject jObj2 = new JSONObject(result);
//                            JSONTokener jsonTokener = new JSONTokener(result);
//                            JSONObject root = (JSONObject) jsonTokener.nextValue();
                        JSONObject merchantObject = new JSONObject(result);
                        JSONObject ownerObject = new JSONObject();
                        JSONObject addressObject = new JSONObject(result2);

                        mCurMurId = authResult.merchantId;

//                        try {
//                            merchantObject = new JSONObject(result);
//                        } catch (JSONException e) {
//                        }
                        try {
                            String busName = merchantObject.getString("name");
                            mCurBusName = busName;
                        } catch (JSONException e) {
                        }
                        try {
                            ownerObject = merchantObject.getJSONObject("owner");
                        } catch (JSONException e) {
                        }
                        try {
                            ownerEmail = ownerObject.getString("email");
                        } catch (JSONException e) {
                        }
//                        try {
//                            addressObject = new JSONObject(result2);
//                        } catch (JSONException e) {
//                        }
                        try {
                            String address1 = addressObject.getString("address1");
                            mCurBusAddress = address1;
                        } catch (JSONException e) {
                        }
                        try {
                            String address2 = addressObject.getString("address2");
                            mCurBusAddressTwo = address2;
                        } catch (JSONException e) {
                        }
                        try {
                            String city = addressObject.getString("city");
                            mCurBusCity = city;
                        } catch (JSONException e) {
                        }
                        try {
                            String country = addressObject.getString("country");
                            mCurBusCountry = country;
                        } catch (JSONException e) {
                        }
                        try {
                            String phoneNumber = addressObject.getString("phoneNumber");
                            mCurBusPone = phoneNumber;
                        } catch (JSONException e) {
                        }
                        try {
                            String state = addressObject.getString("state");
                            mCurBusState = state;
                        } catch (JSONException e) {
                        }
                        try {
                            String zip = addressObject.getString("zip");
                            mCurBusPostal = zip;
                        } catch (JSONException e) {
                        }


//                        String merchantId = root.getString("merchantId");
//                        publishProgress("received merchant id: " + merchantId);

                        // now do another get using the merchant id
//                        String merchantUri = "/v3/merchants/" + merchantId+"/address";
//                        String inventoryUri = "/v2/merchant/" + merchantId + "/inventory/items";
//                        url = authResult.baseUrl + merchantUri + "?access_token=" + authResult.authToken;

//                        publishProgress("requesting merchant using: " + url);
//                        result = httpClient.get(url);

                        //TODO implement check for if the user exists or not
//                        boolean userExists = false;
//                        String checkForUserUrl = authResult.baseUrl + getMerchantUri + authResult.merchantId;//+"/address";
//                        HttpGet checkForUserRequest = new HttpGet(checkForUserUrl);
//                        checkForUserRequest.setHeader("Authorization", "Bearer " + authResult.authToken);
//                        publishProgress("requesting merchant using: " + checkForUserUrl);
//                        String checkForUserResult = httpClient.execute(checkForUserRequest, responseHandler);
//
//                        if(userExists){
//                            Intent finishIntent = new Intent(WebServiceActivity.this, ActivityFinish.class);
//                            startActivity(finishIntent);
//                            finish();
//                        }else {
                            publishProgress("received merchant response: " + result);
                            Intent formIntent = new Intent(WebServiceActivity.this, ActivityForm.class);
                            formIntent.putExtra("mCurBusName", mCurBusName);
                            formIntent.putExtra("mCurMurId", mCurMurId);
                            formIntent.putExtra("mCurBusAddress", mCurBusAddress);
                            formIntent.putExtra("mCurBusAddressTwo", mCurBusAddressTwo);
                            formIntent.putExtra("mCurBusCity", mCurBusCity);
                            formIntent.putExtra("mCurBusCountry", mCurBusCountry);
                            formIntent.putExtra("mCurBusPone", mCurBusPone);
                            formIntent.putExtra("mCurBusState", mCurBusState);
                            formIntent.putExtra("mCurBusPostal", mCurBusPostal);
                            formIntent.putExtra("ownerEmail", ownerEmail);
                            formIntent.putExtra("authToken",mAuthToken);
                            startActivity(formIntent);
                            finish();
                        }


                    }
                } catch (Exception e) {
                    publishProgress("Error retrieving merchant info from server" + e);
                        queryWebService();
                    finish();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mButton.setEnabled(true);
            }
        }

                .execute();
    }

    public static class CustomHttpClient extends DefaultHttpClient {
        private static final int CONNECT_TIMEOUT = 60000;
        private static final int READ_TIMEOUT = 60000;
        private static final int MAX_TOTAL_CONNECTIONS = 5;
        private static final int MAX_CONNECTIONS_PER_ROUTE = 3;
        private static final int SOCKET_BUFFER_SIZE = 8192;
        private static final boolean FOLLOW_REDIRECT = false;
        private static final boolean STALE_CHECKING_ENABLED = true;
        private static final String CHARSET = HTTP.UTF_8;
        private static final HttpVersion HTTP_VERSION = HttpVersion.HTTP_1_1;
        private static final String USER_AGENT = "CustomHttpClient"; // + version

        public static CustomHttpClient getHttpClient() {
            CustomHttpClient httpClient = new CustomHttpClient();
            final HttpParams params = httpClient.getParams();
            HttpProtocolParams.setUserAgent(params, USER_AGENT);
            HttpProtocolParams.setContentCharset(params, CHARSET);
            HttpProtocolParams.setVersion(params, HTTP_VERSION);
            HttpClientParams.setRedirecting(params, FOLLOW_REDIRECT);
            HttpConnectionParams.setConnectionTimeout(params, CONNECT_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, READ_TIMEOUT);
            HttpConnectionParams.setSocketBufferSize(params, SOCKET_BUFFER_SIZE);
            HttpConnectionParams.setStaleCheckingEnabled(params, STALE_CHECKING_ENABLED);
            ConnManagerParams.setTimeout(params, CONNECT_TIMEOUT);
            ConnManagerParams.setMaxTotalConnections(params, MAX_TOTAL_CONNECTIONS);
            ConnManagerParams.setMaxConnectionsPerRoute(params, new ConnPerRouteBean(MAX_CONNECTIONS_PER_ROUTE));

            return httpClient;
        }

        public String get(String url) throws IOException, HttpException {
            String result;
            HttpGet request = new HttpGet(url);
            HttpResponse response = execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    result = EntityUtils.toString(entity);
                } else {
                    throw new HttpException("Received empty body from HTTP response");
                }
            } else {
                throw new HttpException("Received non-OK status from server: " + response.getStatusLine());
            }
            return result;
        }

        @SuppressWarnings("unused")
        public String post(String url, String body) throws IOException, HttpException {
            String result;
            HttpPost request = new HttpPost(url);
            HttpEntity bodyEntity = new StringEntity(body);
            request.setEntity(bodyEntity);
            HttpResponse response = execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    result = EntityUtils.toString(entity);
                } else {
                    throw new HttpException("Received empty body from HTTP response");
                }
            } else {
                throw new HttpException("Received non-OK status from server: " + response.getStatusLine());
            }
            return result;
        }
    }

    private void getMerchant() {

        new AsyncTask<Void, Void, Merchant>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Merchant doInBackground(Void... params) {
                Merchant merchant = null;
                try {
                    CloverAuth.AuthResult authResult = CloverAuth.authenticate(WebServiceActivity.this, mAccount);
                    mAuthToken = authResult.authToken;
                    }
                catch(Exception e){
                        log("Error retrieving merchant info from server" + e);
                    }


                try {

                    merchant = mMerchantConnector.getMerchant();
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (ClientException e) {
                    e.printStackTrace();
                } catch (ServiceException e) {
                    e.printStackTrace();
                } catch (BindingException e) {
                    e.printStackTrace();
                }
                return merchant;
            }

            @Override
            protected void onPostExecute(Merchant merchant) {
                super.onPostExecute(merchant);

                if (!isFinishing()) {
                    // Populate the merchant information
                    if (merchant != null) {
                        Intent formIntent = new Intent(WebServiceActivity.this, ActivityForm.class);

                        formIntent.putExtra("mCurBusName", merchant.getName());
                        formIntent.putExtra("mCurMurId", merchant.getId());
                        formIntent.putExtra("mCurBusAddress", merchant.getAddress().getAddress1());
                        formIntent.putExtra("mCurBusAddressTwo", merchant.getAddress().getAddress2());
                        formIntent.putExtra("mCurBusCity", merchant.getAddress().getCity());
                        formIntent.putExtra("mCurBusCountry", merchant.getAddress().getCountry());
                        formIntent.putExtra("mCurBusPone", merchant.getPhoneNumber());
                        formIntent.putExtra("mCurBusState", merchant.getAddress().getState());
                        formIntent.putExtra("mCurBusPostal", merchant.getAddress().getZip());
                        formIntent.putExtra("ownerEmail", merchant.getSupportEmail());
                        formIntent.putExtra("authToken",mAuthToken);
                        startActivity(formIntent);
                        finish();
                    }
                    else{
                        Intent finishIntent = new Intent(WebServiceActivity.this, ActivityFinish.class);
                        startActivity(finishIntent);
                        finish();
                    }

                }
            }
        }.execute();
    }

    @Override
    public void onStart() {
        // RUN SUPER | REGISTER ACTIVITY AS INSTANTIATED IN APP CLASS

        super.onStart();
        App.activity5 = this;
    }

    @Override
    public void onDestroy() {
        // RUN SUPER | REGISTER ACTIVITY AS NULL IN APP CLASS

        super.onDestroy();
        App.activity5 = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAccount = CloverAccount.getAccount(this);
    }

    @Override
    protected void onPause() {
        disconnect();
        super.onPause();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}